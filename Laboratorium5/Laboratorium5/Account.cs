﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lab5
{
    class Account
    {
        private string firstName;
        private string lastName;
        private string phoneNumber;

        public string FirstName
        {
            get
            {
                return firstName;
            }
            protected set
            {
                firstName = value;
                if (!firstName.All(char.IsLetter)) firstName = "Wrong_value";
            }
        }
        public string LastName
        {
            get
            {
                return lastName;
            }
            protected set
            {
                lastName = value;
                if (!lastName.All(char.IsLetter)) lastName = "Wrong_value";
            }
        }
        public string PhoneNumber
        {
            get
            {
                return phoneNumber;
            }
            protected set
            {
                phoneNumber = value;
                if (phoneNumber.Length != 9) phoneNumber = "Wrong_value";
            }
        }

        public Account(string firstName)
        {
        }

        public Account(string firstName, string lastName, string phoneNumber)
        {
            FirstName = firstName;
            LastName = lastName;
            PhoneNumber = phoneNumber;
        }

        public virtual bool CheckInput()
        {
            if (firstName == "Wrong_value" || lastName == "Wrong_value" || phoneNumber == "Wrong_value") 
                return false;
            else 
                return true;
        }

    }
}
