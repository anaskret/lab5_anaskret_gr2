﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lab5
{
    class Individual : Account
    {
        private string address;

        public string Address
        {
            get
            {
                return address;
            }
            protected set
            {
                address = value;
                if (!address.All(char.IsLetter)) address = "Wrong_value";

            }
        }

        public Individual(string firstName) : base(firstName)
        {
        }

        public Individual(string firstName, string lastName, string phoneNumber, string address) : base(firstName, lastName, phoneNumber)
        {
            Address = address;
        }

        public override bool CheckInput()
        {
            base.CheckInput();
            if(address == "Wrong_value") 
                return false;
            else 
                return true;
        }

        public override string ToString()
        {
            return $"Imie: {FirstName}, Nazwisko: {LastName}\nNr. Telefonu: {PhoneNumber}\nAdres: {Address}\n";
        }
    }
}
