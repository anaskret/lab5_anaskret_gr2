﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lab5
{
    class Company : Account
    {
        private string companyAddress;
        private string companyName;
        private string nip;

        public string CompanyAddress
        {
            get
            {
                return companyAddress;
            }
            private set
            {
                companyAddress = value;
                if (!companyAddress.All(char.IsLetter)) companyAddress = "Wrong_value";

            }
        }
        public string CompanyName
        {
            get
            {
                return companyName;
            }
            private set
            {
                companyName = value;
                if (!companyName.All(char.IsLetter)) companyName = "Wrong_value";

            }
        }
        public string NIP
        {
            get
            {
                return nip;
            }
            private set
            {
                nip = value;
                if (!nip.All(char.IsLetter)) nip = "Wrong_value";

            }
        }

        public Company(string firstName) : base(firstName)
        {
        }

        public Company(string firstName, string lastName, string phoneNumber, string companyAddress, string companyName, string nip) : base(firstName, lastName, phoneNumber)
        {
            this.companyAddress = companyAddress;
            this.companyName = companyName;
            this.nip = nip;

        }

        public override bool CheckInput()
        {
            base.CheckInput();
            if(companyAddress == "Wrong_value" || companyName == "Wrong_value" || nip == "Wrong_value") 
                return false;
            else 
                return true;
        }
        public override string ToString()
        {
            return $"Imie: {FirstName}, Nazwisko: {LastName}\nNr. Telefonu: {PhoneNumber}" +
                $"\nAdres Firmy: {CompanyAddress}, Nazwa Firmy: {CompanyName}\nNIP: {NIP}\n";
        }
    }
}
