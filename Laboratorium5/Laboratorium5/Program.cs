﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Lab5
{
    class Program
    {
        static void Main(string[] args)
        {
            bool loop = true;
            bool check = false;
            int i = 0;


            List<Account> account = new List<Account>();

            
            while (loop == true)
            {
                Console.WriteLine("\n******MENU******");
                Console.WriteLine("1. Wyświetl wszystkie rekordy");
                Console.WriteLine("2. Wyszukaj rekord po nazwisku");
                Console.WriteLine("3. Wyszukaj rekord po nazwie firmy");
                Console.WriteLine("4. Wyszukaj po numerze telefonu");
                Console.WriteLine("5. Wyswietl rekordy dla Individual/Company");
                Console.WriteLine("6. Dodaj nowy wpis");
                Console.WriteLine("7. Koniec");

                int choice;
                while (!int.TryParse(Console.ReadLine(), out choice)) ;

                switch (choice)
                {
                    case 1:
                        foreach(var item in account)
                        {
                            Console.WriteLine(item.ToString());
                        }
                        break;
                    case 2:
                        Console.WriteLine("Podaj nazwisko do wyszukania: ");
                        string searchLastName = Console.ReadLine();
                        foreach(var item in account)
                        {
                            if (item.LastName == searchLastName) Console.WriteLine(item.ToString());
                        }
                        break;
                    case 3:
                        Console.WriteLine("Podaj nazwe firmy do wyszukania: ");
                        string searchName = Console.ReadLine();

                        var p = account.Find(m => (m is Company)?(m as Company).CompanyName == searchName: false);
                        
                        break;
                    case 4:
                        Console.WriteLine("Podaj numer telefonu do wyszukania: ");
                        string searchPhone = Console.ReadLine();
                        foreach (var item in account)
                        {
                            if (item.PhoneNumber == searchPhone) Console.WriteLine(item.ToString());
                        }
                        break;
                    case 5:
                        Console.WriteLine("Wybierz typ konta do wyswietlenia: ");
                        Console.WriteLine("1. Individual; 2. Compnay");
                        int accountType;
                        while (!int.TryParse(Console.ReadLine(), out accountType)) ;
                        if (accountType == 1)
                        {
                            foreach (var item in account)
                            {
                                bool type = item is Individual;
                                if (type == true) Console.WriteLine(item.ToString());
                            }
                        }
                        else
                        {
                            foreach (var item in account)
                            {
                                bool type = item is Company;
                                if (type == true) Console.WriteLine(item.ToString());
                            }
                        }
                        break;
                    case 6:
                        Console.WriteLine("Ile kont chcesz wpisac do rejestru? ");
                        int howMany;
                        while(!int.TryParse(Console.ReadLine(), out howMany));

                        while (howMany > i)
                        {
                            while (check != true)
                            {
                                Console.WriteLine("Podaj imie: ");
                                string firstName = Console.ReadLine();

                                Console.WriteLine("Podaj nazwisko: ");
                                string lastName = Console.ReadLine();

                                Console.WriteLine("Podaj numer telefonu: ");
                                string phoneNumber = Console.ReadLine();

                                Console.WriteLine("Podaj adres(ulice): ");
                                string address = Console.ReadLine();

                                Console.WriteLine("Jaki typ konta chcesz wpisać?");
                                Console.WriteLine("1. Indywidualne; 2. Firmowe");
                                int typ = Convert.ToInt32(Console.ReadLine());
                                switch (typ)
                                {
                                     case 1:
                                        Individual ind = new Individual(firstName, lastName, phoneNumber, address);

                                        if (ind.CheckInput() == true)
                                        {
                                            Console.WriteLine("Udalo sie dodac wpis");
                                            account.Add(ind);
                                            check = true;
                                        }
                                        else
                                        {
                                            check = false;
                                            Console.WriteLine("Conajmniej jedna z wprowadzonych danych była błędna, spróbuj ponownie");
                                        }
                                        break;
                                     case 2:
                                        Console.WriteLine("Podaj nazwe firmy: ");
                                        string companyName = Console.ReadLine();

                                        Console.WriteLine("Podaj NIP firmy (11 cyfr): ");
                                        string nip = Console.ReadLine();

                                        Company com = new Company(firstName, lastName, phoneNumber, address, companyName, nip);

                                        if (com.CheckInput() == true)
                                        {
                                            account.Add(com);
                                            check = true;
                                        }
                                        else
                                        {
                                            check = false;
                                            Console.WriteLine("Conajmniej jedna z wprowadzonych danych była błędna, spróbuj ponownie");
                                        }
                                        break;
                                    default:
                                        Console.WriteLine("Nie wybrales prawidlowego typu");
                                        break;
                                }
                            }
                            i++;
                            check = false;
                        }
                        break;
                    case 7:
                        loop = false;
                        break;
                    default:
                        Console.WriteLine("Wybrales zla opcje");
                        break;
                }

            }
            //Wyszukiwanie: słowa kluczowe IS, AS
        }
    }
}
